package com.acme.ecommercepricingservice.exception;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@AllArgsConstructor
@NoArgsConstructor
public class PriceNotFoundException extends RuntimeException {

    @SuppressWarnings("unused")
	private String message;

    public PriceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}