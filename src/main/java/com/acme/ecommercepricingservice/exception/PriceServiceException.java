package com.acme.ecommercepricingservice.exception;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@AllArgsConstructor
@NoArgsConstructor
public class PriceServiceException extends RuntimeException {

    @SuppressWarnings("unused")
	private String message;

    public PriceServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}