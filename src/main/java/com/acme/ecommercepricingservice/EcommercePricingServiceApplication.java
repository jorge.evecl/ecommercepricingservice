package com.acme.ecommercepricingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.acme.ecommercepricingservice.repository")
@EntityScan(basePackages = "com.acme.ecommercepricingservice.model.entity")
public class EcommercePricingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcommercePricingServiceApplication.class, args);
	}

}
