package com.acme.ecommercepricingservice.service;

import java.time.LocalDateTime;

import com.acme.ecommercepricingservice.model.response.PriceResponse;

public interface PriceService {
	PriceResponse getPriceByParameters(LocalDateTime applicationDate, Long productId, Long brandId);
}
