package com.acme.ecommercepricingservice.service.imp;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acme.ecommercepricingservice.exception.PriceNotFoundException;
import com.acme.ecommercepricingservice.exception.PriceServiceException;
import com.acme.ecommercepricingservice.model.entity.Price;
import com.acme.ecommercepricingservice.model.response.PriceResponse;
import com.acme.ecommercepricingservice.repository.PriceRepository;
import com.acme.ecommercepricingservice.service.PriceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

@Service
public class PriceServiceImp implements PriceService {
	
	 private static final Logger logger = LoggerFactory.getLogger(PriceService.class);

	    @Autowired
	    private PriceRepository priceRepository;

	@Override
	public PriceResponse getPriceByParameters(LocalDateTime applicationDate, Long productId, Long brandId) {
        try {
            List<Price> prices = priceRepository.findPricesByParameters(brandId, productId, applicationDate);
            
            if (prices != null && !prices.isEmpty()) {
                Price finalPrice = selectFinalPrice(prices);
                
                PriceResponse priceResponse = new PriceResponse();
                priceResponse.setProductId(finalPrice.getProductId());
                priceResponse.setBrandId(finalPrice.getBrandId());
                priceResponse.setPriceList(finalPrice.getPriceList());
                priceResponse.setStartDate(finalPrice.getStartDate());
                priceResponse.setEndDate(finalPrice.getEndDate());
                priceResponse.setPrice(finalPrice.getPrice());
                priceResponse.setCurrency(finalPrice.getCurrency());
                
                return priceResponse;
            } else {
                logger.warn("No se encontraron precios para los parámetros proporcionados");
                throw new PriceNotFoundException("No se encontraron precios para los parámetros proporcionados");
            }
        } catch (Exception e) {
            logger.error("Error al obtener precios por parámetros: {}", e.getMessage(), e);
            throw new PriceServiceException("Error al obtener precios por parámetros", e);
        }
    }

    private Price selectFinalPrice(List<Price> prices) {
        Price finalPrice = prices.get(0);
        for (Price price : prices) {
            if (price.getPriority() > finalPrice.getPriority()) {
                finalPrice = price;
            }
        }
        return finalPrice;
    }
}
