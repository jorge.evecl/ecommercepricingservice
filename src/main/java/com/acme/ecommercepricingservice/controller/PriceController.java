package com.acme.ecommercepricingservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.acme.ecommercepricingservice.exception.PriceNotFoundException;
import com.acme.ecommercepricingservice.exception.PriceServiceException;
import com.acme.ecommercepricingservice.model.response.PriceResponse;
import com.acme.ecommercepricingservice.service.PriceService;

import java.time.LocalDateTime;

@RestController
public class PriceController {

    @Autowired
    private PriceService priceService;

    @GetMapping("/prices")
    public ResponseEntity<PriceResponse> getPrice(
            @RequestParam LocalDateTime applicationDate,
            @RequestParam Long productId,
            @RequestParam Long brandId) {

        try {
            PriceResponse priceResponse = priceService.getPriceByParameters(applicationDate, productId, brandId);
            return new ResponseEntity<>(priceResponse, HttpStatus.OK);
        } catch (PriceNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (PriceServiceException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
